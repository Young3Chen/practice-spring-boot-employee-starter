package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.pojo.Company;
import com.thoughtworks.springbootemployee.pojo.Employee;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CompanyRepository {
    private List<Company> companies;


    CompanyRepository() {
        companies = new ArrayList<>();
    }

    public List<Company> getAllCompanies() {
        return companies;
    }

    public Company getCompanyById(Integer id) {
        return companies.stream().filter(company -> company.getId() == id).findFirst().orElse(null);
    }


    public List<Company> getCompaniesPage(Integer page, Integer size) {
        return companies.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public Company createCompany(Company company) {
        company.setId(nextId());
        companies.add(company);
        return company;
    }

    private int nextId() {
        int maxId = companies.stream()
                .mapToInt(Company::getId)
                .max()
                .orElse(0);
        return maxId + 1;
    }

    public Company updateCompany(int id, Company company) {
        Company companyFound = companies.stream()
                .filter(company1 -> company1.getId() == id)
                .findFirst()
                .orElse(null);
        if (companyFound != null) {
            companyFound.setName(company.getName());
        }
        return companyFound;
    }

    public void deleteCompanyById(int companyId) {
        companies.stream()
                .filter(company -> companyId == company.getId())
                .findFirst()
                .ifPresent(company -> companies.remove(company));
    }
}
