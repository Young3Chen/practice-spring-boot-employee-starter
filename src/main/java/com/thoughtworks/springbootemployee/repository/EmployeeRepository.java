package com.thoughtworks.springbootemployee.repository;


import com.thoughtworks.springbootemployee.pojo.Employee;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class EmployeeRepository {
    private List<Employee> employees = new ArrayList<>();

    public EmployeeRepository(List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployeeList() {
        return employees;
    }

    public Employee getEmployeeById(int id) {
        return employees.stream()
                .filter(employee -> employee.getId() == id).findFirst().orElse(null);
    }

    public List<Employee> getEmployeesByGender(@RequestParam String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    public Employee createEmployee(Employee employee) {
        employee.setId(nextId());
        employees.add(employee);
        return employee;
    }

    public Employee updateEmployee(int id, Employee employee) {
        Employee employeeFound = employees.stream()
                .filter(employeeInList -> employeeInList.getId() == id)
                .findFirst()
                .orElse(null);
        if (employeeFound != null) {
            employeeFound.setAge(employee.getAge());
            employeeFound.setSalary(employee.getSalary());
        }
        return employeeFound;
    }

    public void deleteEmployeeById(int id) {
        employees.stream().filter(employee -> employee.getId() == id)
                .findFirst()
                .ifPresent(employee -> employees.remove(employee));
    }

    public List<Employee> getEmployeePage(int page, int size) {
        return employees.stream()
                .skip((long) (page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }

    private int nextId() {
        int maxId = employees.stream()
                .mapToInt(Employee::getId)
                .max()
                .orElse(0);
        return maxId + 1;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public List<Employee> getAllEmployeesByCompanyId(Integer companyId) {
        return employees.stream()
                .filter(employee -> employee.getCompanyId().equals(companyId))
                .collect(Collectors.toList());
    }

}
